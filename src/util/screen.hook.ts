import { useLayoutEffect, useState } from 'react';

export enum ScreenSize {
  Xs = 0,
  Sm = 576,
  Md = 768,
  Lg = 992,
  Xl = 1200,
  Xxl = 1600,
}

const sizes = [
  ScreenSize.Xs,
  ScreenSize.Sm,
  ScreenSize.Md,
  ScreenSize.Lg,
  ScreenSize.Xl,
  ScreenSize.Xxl,
];

function getBreakpoint(width: number): ScreenSize {
  let index = 0;
  while (width > sizes[index + 1]) {
    index++;
  }

  return sizes[index];
}

interface SizeState {
  width: number;
  height: number;
  breakpoint: ScreenSize;
}

export function useScreen() {
  const [size, setSize] = useState<SizeState>({
    width: 0,
    height: 0,
    breakpoint: ScreenSize.Xs,
  });
  useLayoutEffect(() => {
    function updateSize() {
      const width = window.innerWidth;
      const height = window.innerHeight;
      const breakpoint = getBreakpoint(width);
      setSize({ width, height, breakpoint });
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return size;
}
