import React, { createContext, useContext, useState, useCallback } from 'react';

import { ModalMode } from './modal';

export interface ModalState {
  modalMode: ModalMode;
  setMode: (mode: ModalMode) => void;
  setModeWithContext: (mode: ModalMode, context: number | string) => void;
  closeModal: () => void;
  contextId?: number | string;
}

const defaultState: ModalState = {
  modalMode: ModalMode.Closed,
  setMode: () => {},
  setModeWithContext: () => {},
  closeModal: () => {},
};

const ModalContext = createContext<ModalState>(defaultState);

interface StateArgs {
  children: any;
}

export const useModalState = () => useContext(ModalContext);

function ModalProvider({ children }: StateArgs) {
  const [modalMode, updateMode] = useState(ModalMode.Closed);
  const [contextId, setContext] = useState<number | string | undefined>(
    undefined,
  );

  const setMode = useCallback(
    (mode: ModalMode) => {
      setContext(undefined);
      updateMode(mode);
    },
    [updateMode, setContext],
  );

  const setModeWithContext = useCallback(
    (mode: ModalMode, context: number | string) => {
      updateMode(mode);
      setContext(context);
    },
    [updateMode, setContext],
  );

  const closeModal = useCallback(() => {
    setMode(ModalMode.Closed);
  }, [setMode]);

  const state = {
    modalMode,
    closeModal,
    setMode,
    setModeWithContext,
    contextId,
  };

  return (
    <ModalContext.Provider value={state}>{children}</ModalContext.Provider>
  );
}

export default ModalProvider;
