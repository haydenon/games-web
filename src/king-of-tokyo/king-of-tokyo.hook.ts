import { useKingOfTokyoState } from './KingOfTokyoProvider';

export function useKingOfTokyo() {
  const {
    isLoaded,
    isExecuting,
    load,
    dispatch,
    kingOfTokyo,
  } = useKingOfTokyoState();

  return {
    isExecuting,
    isLoaded,
    load,
    kingOfTokyo,
    dispatch,
  };
}
