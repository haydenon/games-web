import React, { useEffect, useState } from 'react';
import { match, Redirect } from 'react-router-dom';

import Loading from '../util/Loading';
import { useKingOfTokyo } from './king-of-tokyo.hook';
import KingOfTokyoProvider from './KingOfTokyoProvider';
import Lobby from './Lobby';
import Game from './game/Game';
import { createHelpers } from './king-of-tokyo.helpers';
import { useNotifications } from '../notification/notification.hook';
import { NetworkError } from '../util/fetch';
import { useGames } from '../game/game.hook';

interface GameArgs {
  lookup: string;
}

const KingOfTokyoGame: React.FC<GameArgs> = ({ lookup }: GameArgs) => {
  const { isLoaded, load, kingOfTokyo } = useKingOfTokyo();
  const { isLobby } = createHelpers(kingOfTokyo);
  const { error } = useNotifications();
  const [notFound, setNotFound] = useState(false);
  const { hasJoined, setJoined } = useGames();

  useEffect(() => {
    if (isLoaded && !hasJoined(lookup)) {
      setJoined(lookup);
    }
  }, [isLoaded, lookup, hasJoined, setJoined]);

  useEffect(() => {
    if (!isLoaded) {
      load(lookup).catch((err: NetworkError) => {
        error(err.message);
        if (err.status === 404) {
          setNotFound(true);
        }
      });
    }
  }, [lookup, isLoaded, load, error]);

  if (notFound) {
    return <Redirect to={{ pathname: '/' }} />;
  }

  if (!isLoaded || !kingOfTokyo) {
    return <Loading></Loading>;
  }

  return isLobby() ? <Lobby lookup={lookup}></Lobby> : <Game></Game>;
};

interface WrapperArgs {
  match: match<{ id: string }>;
}

const KingOfTokyo: React.FC<WrapperArgs> = ({ match }: WrapperArgs) => {
  return (
    <KingOfTokyoProvider>
      <KingOfTokyoGame lookup={match.params.id}></KingOfTokyoGame>
    </KingOfTokyoProvider>
  );
};

export default KingOfTokyo;
