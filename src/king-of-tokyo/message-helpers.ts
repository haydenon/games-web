import { KingOfTokyoPlayer } from './king-of-tokyo';

export enum Prefix {
  Has,
  Is,
}

export const playerName = (
  players: KingOfTokyoPlayer[],
  yourId: string,
  id: string | undefined,
  fill: boolean = false,
): string | undefined => {
  if (id === yourId) {
    return fill ? 'you' : undefined;
  }
  return id && players.find((p) => p.id === id)?.name;
};

export const playerPrefix = (
  prefix: Prefix,
  players: KingOfTokyoPlayer[],
  yourId: string,
  id: string | undefined,
): string => {
  const name = playerName(players, yourId, id);
  if (!name) {
    return prefix === Prefix.Is ? 'You are' : 'You have';
  }

  return prefix === Prefix.Is ? `${name} is` : `${name} has`;
};

export const capitalise = (str: string | undefined): string | undefined => {
  return str ? str[0].toUpperCase() + str.substring(1) : str;
};

export const possessive = (name: string | undefined): string =>
  !name ? 'your' : `${name}'s`;

export const playerList = (
  players: KingOfTokyoPlayer[],
  yourId: string,
  ids: string[],
): string => {
  const name = (id: string): string =>
    playerName(players, yourId, id, true) || 'you';
  if (ids.length < 2) {
    return name(ids[0]);
  }
  const prefix = ids
    .slice(1, ids.length)
    .map((p) => name(p))
    .join(', ');

  return `${prefix} & ${name(ids[0])}`;
};

export const playerListPrefix = (
  players: KingOfTokyoPlayer[],
  yourId: string,
  ids: string[],
  prefix: Prefix,
): string => {
  if (ids.length < 2) {
    return playerPrefix(prefix, players, yourId, ids[0]);
  }

  const name = (id: string): string =>
    playerName(players, yourId, id, true) || 'you';
  const namesPrefix = ids
    .slice(1, ids.length)
    .map((p) => name(p))
    .join(', ');

  return `${namesPrefix} & ${name(ids[0])} have`;
};
