import React from 'react';
import { Card, Button } from 'react-bootstrap';

import './lobby.scss';

import Avatar from './avatars/Avatar';
import { useKingOfTokyo } from './king-of-tokyo.hook';
import { KingOfTokyoPlayer } from './king-of-tokyo';
import { createStartGame } from './actions/start-game.action';
import { createHelpers } from './king-of-tokyo.helpers';
import AvatarPicker from './avatars/AvatarPicker';

interface PlayerArgs {
  isPlayer: boolean;
  player?: KingOfTokyoPlayer;
}

const PlayerCard: React.FC<PlayerArgs> = ({ player, isPlayer }: PlayerArgs) => {
  const cardClass = player ? 'player__filled' : 'player__empty';
  return (
    <li>
      <Card className={`player__card ${cardClass}`}>
        <Card.Body>
          <div className="player__content">
            <span className="player__name">{player?.name ?? '(empty)'}</span>
            {player ? <Avatar avatarType={player.avatar || undefined} /> : null}
          </div>
          {isPlayer ? (
            <div className="player__avatars">
              <AvatarPicker />
            </div>
          ) : null}
        </Card.Body>
      </Card>
    </li>
  );
};

interface LobbyArgs {
  lookup: string;
}

const Lobby: React.FC<LobbyArgs> = ({ lookup }) => {
  const { kingOfTokyo, dispatch, isExecuting } = useKingOfTokyo();
  const { isOwner, playerName, ownerId, playerCount, yourId } = createHelpers(
    kingOfTokyo,
  );

  const startGame = () => {
    dispatch(createStartGame());
  };

  return (
    <div className="lobby__container">
      <h1>Lobby</h1>
      <div className="lobby__content">
        <div className="lobby__panel">
          <ul className="lobby__players">
            {[...(Array(6) as any).keys()].map((idx: number) => {
              const player = kingOfTokyo?.players[idx];
              return (
                <PlayerCard
                  key={idx}
                  player={player}
                  isPlayer={player?.id === yourId()}
                ></PlayerCard>
              );
            })}
          </ul>
          <Card className="lobby__controls">
            <Card.Body>
              {isOwner() ? (
                <Button
                  type="button"
                  color="primary"
                  disabled={isExecuting || playerCount() < 2}
                  onClick={() => startGame()}
                >
                  Start game
                </Button>
              ) : (
                <i className="lobby__waiting">
                  Waiting for {playerName(ownerId())} to start the game.
                </i>
              )}
            </Card.Body>
          </Card>
        </div>

        <div className="lobby__panel">
          <Card className="lobby__join">
            <Card.Body>
              <Card.Title>Join code</Card.Title>
              <span className="join__code">{lookup}</span>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Lobby;
