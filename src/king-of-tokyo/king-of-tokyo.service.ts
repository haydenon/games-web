import { HubConnectionBuilder, HubConnection } from '@microsoft/signalr';
import equal from 'fast-deep-equal';

import { get, buildUrl } from '../util/fetch';
import { Game } from '../game/game';
import { KingOfTokyoView } from './king-of-tokyo';
import { GameEvent, EventType } from './events/event';
import { updateKingOfTokyo } from './king-of-tokyo.reducer';
import { GameAction } from './actions/actions';

const ACTION_TIMEOUT = 5000;

type ResolveReject = [() => void, (err: string) => void];

type GameSubscriber = (
  game: KingOfTokyoView,
  events: GameEvent[],
  error?: string,
) => void;

const gameState: { [id: string]: KingOfTokyoView } = {};

export class KingOfTokyoService {
  private connection: HubConnection;

  private subscribers: GameSubscriber[] = [];

  private connectionInitialised = false;
  private connectionInitialisation: Promise<void>;

  private gameInitialised = false;
  private gameInitialisation?: Promise<void>;

  private resolveExecutes: ResolveReject[] = [];

  constructor() {
    const [connection, connectionInitialisation] = this.buildConnection();
    this.connection = connection;
    this.connectionInitialisation = connectionInitialisation;
  }

  private buildConnection(): [HubConnection, Promise<void>] {
    this.connectionInitialised = false;
    const connection = new HubConnectionBuilder()
      .withAutomaticReconnect()
      .withUrl(buildUrl('/game/hub'))
      .build();

    const connectionInitialisation = connection.start().then(() => {
      this.connectionInitialised = true;
    });

    connection.on('ReceiveEvents', (events: GameEvent[]) =>
      this.handleReceiveEvents(events),
    );

    connection.on('ReceiveActionErrorResponse', (err) => {
      console.error(err);
      this.resolveExecutes.forEach(([_, reject]) => reject(err));
      this.resolveExecutes = [];
    });

    connection.onreconnected(() => {
      for (const gameId in gameState) {
        this.reconnectToGame(gameId);
      }
    });

    return [connection, connectionInitialisation];
  }

  private async restartConnection(): Promise<void> {
    try {
      await this.connection.stop();
      const [connection, connectionInitialisation] = this.buildConnection();
      this.connection = connection;
      this.connectionInitialisation = connectionInitialisation;
      await this.initialiseConnection();
      for (const gameId in gameState) {
        this.reconnectToGame(gameId);
      }
    } catch {}
  }

  public getGameByLookup(lookup: string): Promise<Game> {
    return get<Game>(`/game/lookup/${lookup}`);
  }

  public getGameState(
    id: string,
    lockState: number | undefined = undefined,
  ): Promise<KingOfTokyoView> {
    const url =
      lockState !== undefined
        ? `/game/${id}/state?lockState=${lockState}`
        : `/game/${id}/state`;
    return get<KingOfTokyoView>(url);
  }

  public initialiseConnection = (): Promise<void> =>
    this.connectionInitialised
      ? Promise.resolve(undefined)
      : this.connectionInitialisation;

  public joinGame = async (gameId: string, playerName: string) => {
    await this.connection.send('JoinGame', gameId, playerName);
  };

  public reconnectToGame = async (gameId: string) => {
    await this.connection.send('Reconnect', gameId);
  };

  public subscribeToUpdates = (callback: GameSubscriber) => {
    this.subscribers.push(callback);
    return callback;
  };

  public unsubscribeToUpdates = (callback: GameSubscriber) => {
    const index = this.subscribers.indexOf(callback);
    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  };

  public executeAction = async (action: GameAction): Promise<void> => {
    return new Promise((resolve, reject) => {
      try {
        const timeout = setTimeout(async () => {
          console.warn('Action timed out, restarting connection...');
          await this.restartConnection();
          console.log('Restarted connection, retrying action...');
          this.connection.send('ExecuteAction', action);
        }, ACTION_TIMEOUT);

        const resolveAndClearTimeout = () => {
          clearTimeout(timeout);
          resolve();
        };

        this.resolveExecutes.push([resolveAndClearTimeout, reject]);
        this.connection.send('ExecuteAction', action);
      } catch (err) {
        reject(err);
      }
    });
  };

  private async loadGameStateAfterConnect(gameId: string, lockState: number) {
    let res: any;
    this.gameInitialisation = new Promise((resolve) => {
      res = resolve;
    });
    const state = await this.getGameState(gameId, lockState);
    res();
    this.gameInitialised = true;
    gameState[gameId] = state;
    this.subscribers.forEach((c) => c(state, []));
  }

  private handleReceiveEvents(events: GameEvent[]): void {
    const joinEvents = events.filter(
      (event) => event.eventType === EventType.Reconnect,
    );

    if (joinEvents.length || !gameState[events[0].gameId]) {
      for (const event of events) {
        this.loadGameStateAfterConnect(event.gameId, event.lockState);
      }
    } else {
      this.handleGameEvents(events);
    }
  }

  private async handleGameEvents(events: GameEvent[]) {
    if (!this.gameInitialised && !this.gameInitialisation) {
      throw new Error('Cannot access game before initialised');
    } else if (!this.gameInitialised) {
      await this.gameInitialisation;
    }

    this.resolveExecutes.forEach(([res]) => res());
    this.resolveExecutes = [];

    const id = events[0].gameId;
    const updated = events.reduce(
      (update, event) => updateKingOfTokyo(update, event),
      gameState[id],
    );
    gameState[id] = updated;

    if (process.env.NODE_ENV !== 'production') {
      const actualState = await this.getGameState(id);
      actualState.accessingPlayerId = updated.accessingPlayerId;
      if (!equal(actualState, updated)) {
        console.error(actualState, updated);
        this.subscribers.forEach((c) =>
          c(actualState, events, "Updated state doesn't match server state."),
        );
        return;
      }
    }

    this.subscribers.forEach((c) => c(updated, events));
  }
}
