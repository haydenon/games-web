import cloneDeep from 'clone-deep';

import { KingOfTokyoView } from './king-of-tokyo';
import { GameEvent, EventType, ChangeStatData } from './events/event';
import { GameStartEvent } from './events/game-start.event';
import { PlayerJoinEvent } from './events/player-join.event';
import {
  createPlayerRollStartState,
  PlayerRollStartState,
} from './state/player-roll-start.state';
import { DiceRollType } from './models/dice-roll';
import { PlayerState } from './models/player-state';
import { PlayerRollStartEvent } from './events/player-roll-start.event';
import { DiceRollEvent } from './events/dice-roll.event';
import { StateType, isPlayerState } from './state/state';
import { PlayerRollState } from './state/player-roll.state';
import { ChangePointsEvent } from './events/change-points.event';
import { ChangeHealthEvent } from './events/change-health.event';
import { ChangeEnergyEvent } from './events/change-energy.event';
import { EnteredTokyoEvent } from './events/entered-tokyo.event';
import { PlayerTurnStartEvent } from './events/player-turn-start.event';
import { createPlayerTurnStartState } from './state/player-turn-start.state';
import { BuyCardRequestEvent } from './events/buy-card-request.event';
import { createBuyCardsState } from './state/buy-cards.state';
import { BuyCardEvent } from './events/buy-card.event';
import { LeaveTokyoRequestEvent } from './events/leave-tokyo-request.event';
import { createLeaveTokyo } from './state/leave-tokyo.state';
import { ResetDiscardedCardsEvent } from './events/reset-discarded-cards.event';
import { DiscardCardsEvent } from './events/discard-cards.event';
import { LeaveTokyoEvent } from './events/leave-tokyo.event';
import { GameEndEvent } from './events/game-end.event';
import { createGameEndState } from './state/game-end.state';
import { CardType } from './models/card-type';
import { ChangeAvatarEvent } from './events/change-avatar.event';

export function updateKingOfTokyo(
  game: KingOfTokyoView,
  event: GameEvent,
): KingOfTokyoView {
  const updated = cloneDeep(game);

  updated.lockState = event.lockState;

  switch (event.eventType) {
    case EventType.PlayerJoin:
      handleJoin(updated, event);
      break;
    case EventType.GameStart:
      handleGameStart(updated, event);
      break;
    case EventType.GameEnd:
      handleGameEnd(updated, event);
      break;
    case EventType.PlayerTurnStart:
      handlePlayerTurnStart(updated, event);
      break;
    case EventType.PlayerRollStart:
      handlePlayerRollStart(updated, event);
      break;
    case EventType.DiceRoll:
      handleDiceRoll(updated, event);
      break;
    case EventType.ChangeAvatar:
      handleAvatarChange(updated, event);
      break;
    case EventType.ChangePoints:
      handlePointsChange(updated, event);
      break;
    case EventType.ChangeHealth:
      handleHealthChange(updated, event);
      break;
    case EventType.ChangeEnergy:
      handleEnergyChange(updated, event);
      break;
    case EventType.EnteredTokyo:
      handleEnteredTokyo(updated, event);
      break;
    case EventType.LeaveTokyoRequest:
      handleLeaveTokyoRequest(updated, event);
      break;
    case EventType.LeaveTokyo:
      handleLeaveTokyo(updated, event);
      break;
    case EventType.BuyCardRequest:
      handleBuyCardsRequest(updated, event);
      break;
    case EventType.BuyCard:
      handleBuyCard(updated, event);
      break;
    case EventType.DiscardCards:
      handleDiscardCards(updated, event);
      break;
    case EventType.ResetDiscardedCards:
      handleResetDiscardedCards(updated, event);
      break;
  }

  return updated;
}

function handleJoin(updated: KingOfTokyoView, event: PlayerJoinEvent) {
  updated.players[event.data.playerIndex] = event.data.player;
}

function handleGameStart(updated: KingOfTokyoView, event: GameStartEvent) {
  updated.cardStore = event.data.storeCards;

  type PlayerStateMap = { [id: string]: PlayerState };
  type PlayerInventoriesMap = { [id: string]: CardType[] };

  const playersState = updated.players.reduce((acc, player) => {
    acc[player.id] = { hitPoints: 10, victoryPoints: 0, energy: 0 };
    return acc;
  }, {} as PlayerStateMap);
  updated.playerState = playersState;

  const playersInventories = updated.players.reduce((acc, player) => {
    acc[player.id] = [];
    return acc;
  }, {} as PlayerInventoriesMap);
  updated.playerInventories = playersInventories;
}

function handleGameEnd(updated: KingOfTokyoView, event: GameEndEvent) {
  updated.state = createGameEndState(event.data.player);
}

function handlePlayerTurnStart(
  updated: KingOfTokyoView,
  event: PlayerTurnStartEvent,
) {
  updated.state = createPlayerTurnStartState(event.data.player);
}

function handlePlayerRollStart(
  updated: KingOfTokyoView,
  event: PlayerRollStartEvent,
) {
  updated.state = createPlayerRollStartState(event.data.player);
  const diceRoll = [];
  for (let i = 0; i < 6; i++) {
    diceRoll.push({ diceRoll: DiceRollType.One, wasPreserved: false });
  }
  updated.diceRoll = diceRoll;
}

function handleDiceRoll(updated: KingOfTokyoView, event: DiceRollEvent) {
  const state = updated.state as PlayerRollState | PlayerRollStartState;
  const newState: PlayerRollState =
    state.state === StateType.PlayerRoll
      ? {
          state: StateType.PlayerRoll,
          player: state.player,
          rollNumber: state.rollNumber + 1,
        }
      : {
          state: StateType.PlayerRoll,
          player: state.player,
          rollNumber: 1,
        };

  updated.state = newState;
  updated.diceRoll = event.data.diceRoll;
}

function handleAvatarChange(
  updated: KingOfTokyoView,
  event: ChangeAvatarEvent,
) {
  const { player: playerId, avatar } = event.data;
  const player = updated.players.find((p) => p.id === playerId);
  if (player) {
    player.avatar = avatar;
  }
}

function getPlayerState(state: KingOfTokyoView, statData: ChangeStatData) {
  return state.playerState[statData.changePlayer];
}

function handlePointsChange(
  updated: KingOfTokyoView,
  event: ChangePointsEvent,
) {
  const state = getPlayerState(updated, event.data);
  state.victoryPoints += event.data.points;
}

function handleHealthChange(
  updated: KingOfTokyoView,
  event: ChangeHealthEvent,
) {
  const state = getPlayerState(updated, event.data);
  state.hitPoints += event.data.hitPoints;
}

function handleEnergyChange(
  updated: KingOfTokyoView,
  event: ChangeEnergyEvent,
) {
  const state = getPlayerState(updated, event.data);
  state.energy += event.data.energy;
}

function handleEnteredTokyo(
  updated: KingOfTokyoView,
  event: EnteredTokyoEvent,
) {
  if (event.data.wasTokyoBay) {
    updated.playerInTokyoBay = event.data.player;
  } else {
    updated.playerInTokyoCity = event.data.player;
  }
}

function handleLeaveTokyoRequest(
  updated: KingOfTokyoView,
  event: LeaveTokyoRequestEvent,
) {
  updated.state = createLeaveTokyo(
    event.data.leaveRequestPlayers,
    event.data.nextState,
  );
}

function handleLeaveTokyo(updated: KingOfTokyoView, event: LeaveTokyoEvent) {
  const id = event.data.player;
  if (updated.playerInTokyoCity === id && event.data.leavingTokyo) {
    updated.playerInTokyoCity = null;
  } else if (updated.playerInTokyoBay === id && event.data.leavingTokyo) {
    updated.playerInTokyoBay = null;
  }

  const state = updated.state;
  if (state.state !== StateType.LeaveTokyo) {
    return;
  }

  const newRequestPlayers = state.leaveRequestPlayers.filter((p) => p !== id);

  updated.state = createLeaveTokyo(newRequestPlayers, state.nextState);
}

function handleBuyCardsRequest(
  updated: KingOfTokyoView,
  event: BuyCardRequestEvent,
) {
  updated.state = createBuyCardsState(event.data.player);
}

function handleBuyCard(updated: KingOfTokyoView, event: BuyCardEvent) {
  if (event.data.card.isDiscard) {
    updated.discardedCards.push(event.data.card.cardType);
  } else {
    updated.playerInventories[event.data.player].push(event.data.card.cardType);
  }
  updated.cardStore[event.data.index] = event.data.replacementCard;
  updated.playerState[event.data.player].energy -= event.data.card.energyCost;
}

function handleDiscardCards(
  updated: KingOfTokyoView,
  event: DiscardCardsEvent,
) {
  const gameState = updated.state;
  const playerId = isPlayerState(gameState) ? gameState.player : '';
  updated.playerState[playerId].energy -= 2;

  updated.discardedCards = [
    ...updated.discardedCards,
    ...updated.cardStore.map((c) => c.cardType),
  ];
  updated.cardStore = event.data.cardStore;
}

function handleResetDiscardedCards(
  updated: KingOfTokyoView,
  _: ResetDiscardedCardsEvent,
) {
  updated.discardedCards = [];
}
