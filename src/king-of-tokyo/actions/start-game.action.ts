import { KotAction, ActionType, ActionCreator } from './actions';

export interface StartGame extends KotAction {
  actionType: ActionType.StartGame;
}

export const createStartGame = (): ActionCreator => (
  gameId: string,
): StartGame => ({
  actionType: ActionType.StartGame,
  gameId,
});
