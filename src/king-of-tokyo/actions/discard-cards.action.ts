import { KotAction, ActionType, ActionCreator } from './actions';

export interface DiscardCards extends KotAction {
  actionType: ActionType.DiscardCards;
}

export const createDiscardCards = (): ActionCreator => (
  gameId: string,
): DiscardCards => ({
  actionType: ActionType.DiscardCards,
  gameId,
});
