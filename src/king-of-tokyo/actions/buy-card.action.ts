import { KotAction, ActionType, ActionCreator } from './actions';
import { CardType } from '../models/card-type';

export interface BuyCard extends KotAction {
  actionType: ActionType.BuyCard;
  parameters: {
    cardType: CardType;
  };
}

export const createBuyCard = (cardType: CardType): ActionCreator => (
  gameId: string,
): BuyCard => ({
  actionType: ActionType.BuyCard,
  gameId,
  parameters: {
    cardType,
  },
});
