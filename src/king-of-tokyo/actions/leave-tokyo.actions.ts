import { KotAction, ActionType, ActionCreator } from './actions';

export interface LeaveTokyo extends KotAction {
  actionType: ActionType.LeaveTokyo;
  parameters: {
    leaveTokyo: boolean;
  };
}

export const createLeaveTokyo = (leaveTokyo: boolean): ActionCreator => (
  gameId: string,
): LeaveTokyo => ({
  actionType: ActionType.LeaveTokyo,
  gameId,
  parameters: {
    leaveTokyo,
  },
});
