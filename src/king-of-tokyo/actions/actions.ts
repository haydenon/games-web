import { StartGame } from './start-game.action';
import { RollDice } from './roll.action';
import { AcceptDice } from './accept-dice.action';
import { FinishCardBuying } from './finish-card-buying.action';
import { DiscardCards } from './discard-cards.action';
import { BuyCard } from './buy-card.action';
import { LeaveTokyo } from './leave-tokyo.actions';
import { StartTurn } from './start-turn.action';
import { ChangeAvatar } from './change-avatar.action';

export enum ActionType {
  ChangeAvatar = 'ChangeAvatar',
  StartGame = 'StartGame',
  StartTurn = 'StartTurn',
  RollDice = 'RollDice',
  AcceptDice = 'AcceptDice',
  FinishCardBuying = 'FinishCardBuying',
  DiscardCards = 'DiscardCards',
  BuyCard = 'BuyCard',
  LeaveTokyo = 'LeaveTokyo',
}

export interface KotAction {
  actionType: ActionType;
  gameId: string;
}

export type GameAction =
  | ChangeAvatar
  | StartGame
  | StartTurn
  | RollDice
  | AcceptDice
  | FinishCardBuying
  | DiscardCards
  | BuyCard
  | LeaveTokyo;

export type ActionCreator = (gameId: string) => GameAction;
