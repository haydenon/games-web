import { KotAction, ActionType, ActionCreator } from './actions';

export interface AcceptDice extends KotAction {
  actionType: ActionType.AcceptDice;
}

export const createAcceptDice = (): ActionCreator => (
  gameId: string,
): AcceptDice => ({
  actionType: ActionType.AcceptDice,
  gameId,
});
