import { KotAction, ActionType, ActionCreator } from './actions';

export interface StartTurn extends KotAction {
  actionType: ActionType.StartTurn;
}

export const createStartTurn = (): ActionCreator => (
  gameId: string,
): StartTurn => ({
  actionType: ActionType.StartTurn,
  gameId,
});
