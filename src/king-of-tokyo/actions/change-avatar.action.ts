import { KotAction, ActionType, ActionCreator } from './actions';
import { AvatarType } from '../avatars/avatar-type';

export interface ChangeAvatar extends KotAction {
  actionType: ActionType.ChangeAvatar;
  parameters: {
    avatar: AvatarType | null;
  };
}

export const createChangeAvatar = (
  avatar: AvatarType | null,
): ActionCreator => (gameId: string): ChangeAvatar => ({
  actionType: ActionType.ChangeAvatar,
  gameId,
  parameters: {
    avatar,
  },
});
