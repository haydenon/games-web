import { KotAction, ActionType, ActionCreator } from './actions';

export interface FinishCardBuying extends KotAction {
  actionType: ActionType.FinishCardBuying;
}

export const createFinishCardBuying = (): ActionCreator => (
  gameId: string,
): FinishCardBuying => ({
  actionType: ActionType.FinishCardBuying,
  gameId,
});
