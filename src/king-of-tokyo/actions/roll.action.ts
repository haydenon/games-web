import { KotAction, ActionType, ActionCreator } from './actions';

export interface RollDice extends KotAction {
  actionType: ActionType.RollDice;
  parameters: {
    diceToKeep: boolean[];
  };
}

export const createRollDice = (diceToKeep: boolean[]): ActionCreator => (
  gameId: string,
): RollDice => ({
  actionType: ActionType.RollDice,
  gameId,
  parameters: {
    diceToKeep,
  },
});
