import React, {
  createContext,
  useContext,
  useState,
  useEffect,
  useCallback,
} from 'react';

import { KingOfTokyoView } from './king-of-tokyo';
import { useNotifications } from '../notification/notification.hook';
import { KingOfTokyoService } from './king-of-tokyo.service';
import { getEventMessage } from './events/event-messages';
import { usePlayer } from '../game/player.hook';
import { ActionCreator } from './actions/actions';

const kingOfTokyoService = new KingOfTokyoService();

export interface KingOfTokyoState {
  kingOfTokyo?: KingOfTokyoView;
  isExecuting: boolean;
  isLoaded: boolean;
  dispatch: (action: ActionCreator) => Promise<void>;
  load: (id: string) => Promise<void>;
}

const defaultState: KingOfTokyoState = {
  isExecuting: false,
  isLoaded: false,
  dispatch: () => Promise.resolve(),
  load: () => Promise.resolve(),
};

const StateContext = createContext<KingOfTokyoState>(defaultState);

interface StateArgs {
  children: any;
}

export const useKingOfTokyoState = () => useContext(StateContext);

function KingOfTokyoProvider({ children }: StateArgs) {
  const [gameId, setGameId] = useState('');
  const [kingOfTokyo, setKingOfTokyo] = useState<KingOfTokyoView | undefined>(
    undefined,
  );
  const [isLoaded, setLoaded] = useState(false);
  const [isExecuting, setExecuting] = useState(false);
  const { playerName } = usePlayer();

  const { error, info } = useNotifications();

  useEffect(() => {
    const subscriber = kingOfTokyoService.subscribeToUpdates(
      (kingOfTokyoView, events, err) => {
        if (!isLoaded && kingOfTokyoView) {
          setLoaded(true);
        }

        const messages = getEventMessage(events, kingOfTokyoView);
        if (messages.length > 0) {
          info(messages, 2500);
        }

        if (err) {
          error(err);
        }

        setKingOfTokyo(kingOfTokyoView);
      },
    );

    return () => kingOfTokyoService.unsubscribeToUpdates(subscriber);
  }, [error, info, setKingOfTokyo, isLoaded]);

  const load = useCallback(
    async (id: string) => {
      if (!playerName) {
        return;
      }

      const game = await kingOfTokyoService.getGameByLookup(id);
      setGameId(game.id);
      const currentState = await kingOfTokyoService.getGameState(game.id);
      await kingOfTokyoService.initialiseConnection();

      const hasJoined = currentState.players.some(
        (p) => p.id === currentState.accessingPlayerId,
      );

      await (hasJoined
        ? kingOfTokyoService.reconnectToGame(game.id)
        : kingOfTokyoService.joinGame(game.id, playerName));
    },
    [playerName, setGameId],
  );

  const dispatch = useCallback(
    async (actionCreator: ActionCreator) => {
      try {
        setExecuting(true);
        await kingOfTokyoService.executeAction(actionCreator(gameId));
      } catch (err) {
        error(err);
      } finally {
        setExecuting(false);
      }
    },
    [gameId, error],
  );

  const state = {
    kingOfTokyo,
    isExecuting,
    isLoaded,
    dispatch,
    load,
  };

  return (
    <StateContext.Provider value={state}>{children}</StateContext.Provider>
  );
}

export default KingOfTokyoProvider;
