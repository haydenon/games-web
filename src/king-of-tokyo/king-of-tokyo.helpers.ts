import { KingOfTokyoView, KingOfTokyoPlayer } from './king-of-tokyo';
import { PlayerState } from './models/player-state';
import { StateType, isPlayerState } from './state/state';
import { CardType } from './models/card-type';

export function createHelpers(kingOfTokyo?: KingOfTokyoView) {
  const playerName = (id?: string) =>
    id && kingOfTokyo?.players.find((p) => p.id === id)?.name;

  const isOwner = () =>
    kingOfTokyo &&
    kingOfTokyo.accessingPlayerId === kingOfTokyo.creatorPlayerId;

  const ownerId = () => kingOfTokyo?.creatorPlayerId;

  const yourId = () => kingOfTokyo?.accessingPlayerId;

  const player = (id?: string): KingOfTokyoPlayer | undefined =>
    kingOfTokyo?.players.find((player) => player.id === id);

  const playerState = (id?: string): PlayerState =>
    (id && kingOfTokyo?.playerState[id]) || {
      hitPoints: 0,
      victoryPoints: 0,
      energy: 0,
    };

  const playerCount = () => kingOfTokyo?.players.length || 0;

  const alivePlayerCount = () =>
    kingOfTokyo?.players.filter((p) => !isDead(p.id)).length || 0;

  const isLobby = () => kingOfTokyo?.state.state === StateType.Lobby;

  const cardStore = () => kingOfTokyo?.cardStore || [];

  const diceRoll = () => kingOfTokyo?.diceRoll || [];

  const others = (): KingOfTokyoPlayer[] => {
    const players = kingOfTokyo?.players || [];
    const yourIndex = players.findIndex(
      (p) => p.id === kingOfTokyo?.accessingPlayerId,
    );
    if (yourIndex === -1) {
      return players;
    }

    return [...players.slice(yourIndex + 1), ...players.slice(0, yourIndex)];
  };

  const tokyoCityPlayer = () => kingOfTokyo?.playerInTokyoCity || undefined;

  const tokyoBayPlayer = () => kingOfTokyo?.playerInTokyoBay || undefined;

  const isState = (state: StateType, ...otherStates: StateType[]) => {
    const allStates = [state, ...otherStates];
    return allStates.some((s) => kingOfTokyo?.state.state === s);
  };

  const turnPlayer = (): string | undefined => {
    if (!kingOfTokyo) {
      return undefined;
    }
    const state = kingOfTokyo.state;
    return isPlayerState(state) ? state.player : undefined;
  };

  const yourTurn = (): boolean => {
    if (!kingOfTokyo) {
      return false;
    }
    const state = kingOfTokyo.state;
    return isPlayerState(state) && state.player === yourId();
  };

  const rollCount = (): number => {
    if (!kingOfTokyo) {
      return -1;
    }
    const state = kingOfTokyo.state;

    if (state.state === StateType.PlayerRollStart) {
      return 0;
    } else if (state.state === StateType.PlayerRoll) {
      return state.rollNumber;
    } else {
      return -1;
    }
  };

  const isRequestingTokyoLeave = (): boolean => {
    if (!kingOfTokyo) {
      return false;
    }

    const id = yourId();
    const state = kingOfTokyo.state;

    return (
      state.state === StateType.LeaveTokyo &&
      state.leaveRequestPlayers.some((p) => p === id)
    );
  };

  const isDead = (id?: string): boolean => playerState(id).hitPoints <= 0;

  const inventory = (id?: string): CardType[] =>
    (id && kingOfTokyo?.playerInventories[id]) || [];

  return {
    ownerId,
    isOwner,
    playerName,
    playerCount,
    alivePlayerCount,
    isLobby,
    yourId,
    playerState,
    player,
    cardStore,
    diceRoll,
    others,
    tokyoCityPlayer,
    tokyoBayPlayer,
    isState,
    yourTurn,
    turnPlayer,
    rollCount,
    isRequestingTokyoLeave,
    isDead,
    inventory,
  };
}
