export enum ModalMode {
  Closed,
  Inventory,
  CardStore,
  PlayerTurnStart,
  GameEnd,
}
