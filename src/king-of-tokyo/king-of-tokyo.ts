import { KingOfTokyoState } from './state/state';
import { GameCard } from './models/game-card';
import { DiceRoll } from './models/dice-roll';
import { PlayerState } from './models/player-state';
import { CardType } from './models/card-type';
import { AvatarType } from './avatars/avatar-type';

export interface KingOfTokyoView {
  creatorPlayerId: string;
  accessingPlayerId: string;
  lockState: number;
  state: KingOfTokyoState;
  players: KingOfTokyoPlayer[];
  playerState: { [id: string]: PlayerState };
  playerInventories: { [id: string]: CardType[] };
  playerInTokyoCity: string | null;
  playerInTokyoBay: string | null;
  cardStore: GameCard[];
  discardedCards: string[];
  diceRoll: DiceRoll[];
}

export interface KingOfTokyoPlayer {
  id: string;
  name: string;
  avatar: AvatarType | null;
}
