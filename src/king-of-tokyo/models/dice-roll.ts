export enum DiceRollType {
  One = 'One',
  Two = 'Two',
  Three = 'Three',
  Heart = 'Heart',
  Energy = 'Energy',
  Claw = 'Claw',
}

export interface DiceRoll {
  diceRoll: DiceRollType;
  wasPreserved: boolean;
}
