export interface PlayerState {
  hitPoints: number;
  victoryPoints: number;
  energy: number;
}
