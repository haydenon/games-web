import { CardType } from './card-type';

export interface GameCard {
  cardType: CardType;
  energyCost: number;
  isDiscard: boolean;
}

export function cardName(type: CardType): string {
  return type.replace(/([A-Z])/g, ' $1').trim();
}
