import { StateType, KotPlayerState } from './state';

export interface PlayerRollState extends KotPlayerState {
  state: StateType.PlayerRoll;
  rollNumber: number;
}
