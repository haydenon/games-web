import { KingOfTokyoView } from '../king-of-tokyo';
import { StateType } from './state';
import {
  playerPrefix,
  Prefix,
  playerName,
  capitalise,
  possessive,
} from '../message-helpers';

export function statusSummary(kingOfTokyo?: KingOfTokyoView): string {
  if (!kingOfTokyo) {
    return '';
  }

  const state = kingOfTokyo.state;
  const yourId = kingOfTokyo.accessingPlayerId;
  const prefix = (id: string | undefined, prefix: Prefix) =>
    playerPrefix(prefix, kingOfTokyo.players, yourId, id);
  const name = (id: string | undefined, fill: boolean = true) =>
    playerName(kingOfTokyo.players, yourId, id) || (fill ? 'you' : undefined);
  switch (state.state) {
    case StateType.BuyCards:
      return `${prefix(state.player, Prefix.Is)} buying cards`;
    case StateType.GameEnd:
      return state.winningPlayer
        ? `${prefix(state.winningPlayer, Prefix.Has)} won the game`
        : 'The game is a tie';
    case StateType.LeaveTokyo:
      return state.leaveRequestPlayers.length > 1
        ? `${capitalise(name(state.leaveRequestPlayers[0]))} & ${name(
            state.leaveRequestPlayers[1],
          )} are choosing whether to stay in Tokyo`
        : `${prefix(
            state.leaveRequestPlayers[0],
            Prefix.Is,
          )} choosing whether to stay in Tokyo`;
    case StateType.Lobby:
      return 'Lobby';
    case StateType.PlayerRoll:
    case StateType.PlayerRollStart:
      return `It is ${possessive(name(state.player, false))} turn to roll`;
    case StateType.PlayerTurnStart:
      return `It is now ${possessive(name(state.player, false))} turn`;
    default:
      console.error(`State summary not handled for ${(state as any).state}`);
      return '';
  }
}
