import { KotState, StateType } from './state';

export interface LeaveTokyoState extends KotState {
  state: StateType.LeaveTokyo;
  leaveRequestPlayers: string[];
  nextState: string;
}

export const createLeaveTokyo = (
  leaveRequestPlayers: string[],
  nextState: string,
): LeaveTokyoState => ({
  state: StateType.LeaveTokyo,
  leaveRequestPlayers,
  nextState,
});
