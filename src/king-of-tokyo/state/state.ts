import { PlayerRollStartState } from './player-roll-start.state';
import { PlayerRollState } from './player-roll.state';
import { PlayerTurnStartState } from './player-turn-start.state';
import { BuyCardsState } from './buy-cards.state';
import { LeaveTokyoState } from './leave-tokyo.state';
import { GameEndState } from './game-end.state';

export enum StateType {
  Lobby = 'Lobby',
  PlayerTurnStart = 'PlayerTurnStart',
  PlayerRollStart = 'PlayerRollStart',
  PlayerRoll = 'PlayerRoll',
  BuyCards = 'BuyCards',
  LeaveTokyo = 'LeaveTokyo',
  GameEnd = 'GameEnd',
}

export interface KotState {
  state: StateType;
}

export const isPlayerState = (state: KotState): state is KotPlayerState =>
  (state as any).player !== undefined;

export interface KotPlayerState extends KotState {
  player: string;
}

interface LobbyState extends KotState {
  state: StateType.Lobby;
}

export type KingOfTokyoState =
  | LobbyState
  | PlayerTurnStartState
  | PlayerRollStartState
  | PlayerRollState
  | BuyCardsState
  | LeaveTokyoState
  | GameEndState;
