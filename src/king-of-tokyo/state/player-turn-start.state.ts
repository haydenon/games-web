import { StateType, KotPlayerState } from './state';

export interface PlayerTurnStartState extends KotPlayerState {
  state: StateType.PlayerTurnStart;
}

export const createPlayerTurnStartState = (
  playerId: string,
): PlayerTurnStartState => ({
  state: StateType.PlayerTurnStart,
  player: playerId,
});
