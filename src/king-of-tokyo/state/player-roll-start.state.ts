import { KotPlayerState, StateType } from './state';

export interface PlayerRollStartState extends KotPlayerState {
  state: StateType.PlayerRollStart;
}

export const createPlayerRollStartState = (
  playerId: string,
): PlayerRollStartState => ({
  state: StateType.PlayerRollStart,
  player: playerId,
});
