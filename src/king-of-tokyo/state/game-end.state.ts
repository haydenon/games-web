import { StateType, KotState } from './state';

export interface GameEndState extends KotState {
  state: StateType.GameEnd;
  winningPlayer: string | null;
  isTie: boolean;
}

export const createGameEndState = (winningPlayer?: string): GameEndState => ({
  state: StateType.GameEnd,
  winningPlayer: winningPlayer || null,
  isTie: !winningPlayer,
});
