import { KotPlayerState, StateType } from './state';

export interface BuyCardsState extends KotPlayerState {
  state: StateType.BuyCards;
}

export const createBuyCardsState = (playerId: string): BuyCardsState => ({
  state: StateType.BuyCards,
  player: playerId,
});
