import { KotEvent, EventType } from './event';

export interface PlayerTurnStartEvent extends KotEvent {
  eventType: EventType.PlayerTurnStart;
  data: {
    player: string;
  };
}
