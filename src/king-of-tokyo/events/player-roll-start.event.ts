import { KotEvent, EventType } from './event';

export interface PlayerRollStartEvent extends KotEvent {
  eventType: EventType.PlayerRollStart;
  data: {
    player: string;
  };
}
