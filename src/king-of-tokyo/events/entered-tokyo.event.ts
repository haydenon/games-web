import { KotEvent, EventType } from './event';

export interface EnteredTokyoEvent extends KotEvent {
  eventType: EventType.EnteredTokyo;
  data: {
    player: string;
    wasTokyoBay: boolean;
  };
}
