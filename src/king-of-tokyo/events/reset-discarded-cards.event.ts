import { KotEvent, EventType } from './event';

export interface ResetDiscardedCardsEvent extends KotEvent {
  eventType: EventType.ResetDiscardedCards;
}
