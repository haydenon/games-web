import { KotEvent, EventType } from './event';
import { DiceRoll } from '../models/dice-roll';

export interface DiceRollEvent extends KotEvent {
  eventType: EventType.DiceRoll;
  data: {
    diceRoll: DiceRoll[];
  };
}
