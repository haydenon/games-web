import { GameEvent, EventType } from './event';
import { KingOfTokyoView } from '../king-of-tokyo';
import { BuyCardEvent } from './buy-card.event';
import { cardName } from '../models/game-card';
import { DiscardCardsEvent } from './discard-cards.event';
import { LeaveTokyoEvent } from './leave-tokyo.event';
import { EnteredTokyoEvent } from './entered-tokyo.event';
import { isPlayerState } from '../state/state';
import { ChangeHealthEvent } from './change-health.event';
import { PlayerState } from '../models/player-state';
import { capitalise, playerListPrefix, Prefix } from '../message-helpers';

export function getEventMessage(
  events: GameEvent[],
  kingOfTokyo: KingOfTokyoView,
): string[] {
  const players = kingOfTokyo.players;
  const yourId = kingOfTokyo.accessingPlayerId;
  const state = kingOfTokyo.state;
  const currentPlayerId = isPlayerState(state) ? state.player : '';

  const messages: string[] = [];

  const playerState = (id: string): PlayerState =>
    kingOfTokyo.playerState[id] || {
      energy: 0,
      hitPoints: 0,
      victoryPoints: 0,
    };

  const getEvent = <T>(type: EventType): T | undefined =>
    events.find((e) => e.eventType === type) as T | undefined;

  const getEvents = <T>(type: EventType): T[] =>
    (events.filter((e) => e.eventType === type) as any[]) as T[];

  const playerName = (id: string) =>
    id === yourId ? 'You' : players.find((p) => p.id === id)?.name || '';

  const buyCard = getEvent<BuyCardEvent>(EventType.BuyCard);
  if (buyCard) {
    messages.push(
      `${playerName(buyCard.data.player)} bought ${cardName(
        buyCard.data.card.cardType,
      )}`,
    );
  }

  const discard = getEvent<DiscardCardsEvent>(EventType.DiscardCards);
  if (discard) {
    messages.push(`${playerName(currentPlayerId)} discarded the store`);
  }

  const leave = getEvent<LeaveTokyoEvent>(EventType.LeaveTokyo);
  if (leave && leave.data.leavingTokyo) {
    messages.push(`${playerName(leave.data.player)} left Tokyo`);
  }

  const enter = getEvent<EnteredTokyoEvent>(EventType.EnteredTokyo);
  if (enter) {
    messages.push(`${playerName(enter.data.player)} entered Tokyo`);
  }

  const healthChanges = getEvents<ChangeHealthEvent>(EventType.ChangeHealth);
  const whereDead = healthChanges.filter(
    (h) => playerState(h.data.changePlayer).hitPoints <= 0,
  );
  if (whereDead.length > 0) {
    const deadIds = whereDead.map((d) => d.data.changePlayer);
    messages.push(
      `${capitalise(
        playerListPrefix(players, yourId, deadIds, Prefix.Has),
      )} died`,
    );
  }

  return messages;
}
