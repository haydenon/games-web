import { PlayerJoinEvent } from './player-join.event';
import { GameStartEvent } from './game-start.event';
import { PlayerRollStartEvent } from './player-roll-start.event';
import { DiceRollEvent } from './dice-roll.event';
import { ChangePointsEvent } from './change-points.event';
import { ChangeHealthEvent } from './change-health.event';
import { ChangeEnergyEvent } from './change-energy.event';
import { EnteredTokyoEvent } from './entered-tokyo.event';
import { PlayerTurnStartEvent } from './player-turn-start.event';
import { BuyCardRequestEvent } from './buy-card-request.event';
import { BuyCardEvent } from './buy-card.event';
import { LeaveTokyoRequestEvent } from './leave-tokyo-request.event';
import { ResetDiscardedCardsEvent } from './reset-discarded-cards.event';
import { DiscardCardsEvent } from './discard-cards.event';
import { LeaveTokyoEvent } from './leave-tokyo.event';
import { GameEndEvent } from './game-end.event';
import { ChangeAvatarEvent } from './change-avatar.event';

export enum EventType {
  PlayerJoin = 'PlayerJoin',
  Reconnect = 'Reconnect',
  GameStart = 'GameStart',
  PlayerTurnStart = 'PlayerTurnStart',
  PlayerRollStart = 'PlayerRollStart',
  DiceRoll = 'DiceRoll',
  ChangeAvatar = 'ChangeAvatar',
  ChangePoints = 'ChangePoints',
  ChangeHealth = 'ChangeHealth',
  ChangeEnergy = 'ChangeEnergy',
  EnteredTokyo = 'EnteredTokyo',
  LeaveTokyoRequest = 'LeaveTokyoRequest',
  LeaveTokyo = 'LeaveTokyo',
  BuyCardRequest = 'BuyCardRequest',
  BuyCard = 'BuyCard',
  DiscardCards = 'DiscardCards',
  ResetDiscardedCards = 'ResetDiscardedCards',
  GameEnd = 'GameEnd',
}

export interface KotEvent {
  eventType: EventType;
  gameId: string;
  lockState: number;
}

export interface ChangeStatData {
  cardSources: string[];
  executingPlayer: string;
  changePlayer: string;
}

interface Reconnect extends KotEvent {
  data: {};
  eventType: EventType.Reconnect;
  gameId: string;
  lockState: number;
}

export type GameEvent =
  | Reconnect
  | PlayerJoinEvent
  | GameStartEvent
  | GameEndEvent
  | PlayerTurnStartEvent
  | PlayerRollStartEvent
  | DiceRollEvent
  | ChangeAvatarEvent
  | ChangePointsEvent
  | ChangeHealthEvent
  | ChangeEnergyEvent
  | EnteredTokyoEvent
  | LeaveTokyoRequestEvent
  | LeaveTokyoEvent
  | BuyCardRequestEvent
  | BuyCardEvent
  | DiscardCardsEvent
  | ResetDiscardedCardsEvent;
