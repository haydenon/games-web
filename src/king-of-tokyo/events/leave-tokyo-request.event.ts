import { KotEvent, EventType } from './event';

export interface LeaveTokyoRequestEvent extends KotEvent {
  eventType: EventType.LeaveTokyoRequest;
  data: {
    leaveRequestPlayers: string[];
    nextState: string;
  };
}
