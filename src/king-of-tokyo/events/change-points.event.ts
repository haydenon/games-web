import { KotEvent, EventType, ChangeStatData } from './event';

export interface ChangeData extends ChangeStatData {
  points: number;
}

export interface ChangePointsEvent extends KotEvent {
  eventType: EventType.ChangePoints;
  data: ChangeData;
}
