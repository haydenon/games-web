import { KingOfTokyoPlayer } from '../king-of-tokyo';
import { KotEvent, EventType } from './event';

export interface PlayerJoinEvent extends KotEvent {
  data: {
    player: KingOfTokyoPlayer;
    playerIndex: number;
  };
  eventType: EventType.PlayerJoin;
  gameId: string;
  lockState: number;
}
