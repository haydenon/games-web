import { KotEvent, EventType } from './event';
import { GameCard } from '../models/game-card';

export interface BuyCardEvent extends KotEvent {
  eventType: EventType.BuyCard;
  data: {
    player: string;
    card: GameCard;
    index: number;
    replacementCard: GameCard;
  };
}
