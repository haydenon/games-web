import { KotEvent, EventType } from './event';
import { GameCard } from '../models/game-card';

export interface GameStartEvent extends KotEvent {
  data: {
    startingPlayerIndex: number;
    storeCards: GameCard[];
  };
  eventType: EventType.GameStart;
}
