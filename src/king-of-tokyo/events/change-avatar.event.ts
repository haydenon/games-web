import { AvatarType } from '../avatars/avatar-type';
import { KotEvent, EventType } from './event';

export interface ChangeAvatarEvent extends KotEvent {
  eventType: EventType.ChangeAvatar;
  data: {
    player: string;
    avatar: AvatarType | null;
  };
}
