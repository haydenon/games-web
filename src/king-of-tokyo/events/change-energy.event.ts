import { KotEvent, EventType, ChangeStatData } from './event';

export interface ChangeData extends ChangeStatData {
  energy: number;
}

export interface ChangeEnergyEvent extends KotEvent {
  eventType: EventType.ChangeEnergy;
  data: ChangeData;
}
