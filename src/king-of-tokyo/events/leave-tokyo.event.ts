import { KotEvent, EventType } from './event';

export interface LeaveTokyoEvent extends KotEvent {
  eventType: EventType.LeaveTokyo;
  data: {
    player: string;
    leavingTokyo: boolean;
  };
}
