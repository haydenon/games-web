import { KotEvent, EventType, ChangeStatData } from './event';

export interface ChangeData extends ChangeStatData {
  hitPoints: number;
}

export interface ChangeHealthEvent extends KotEvent {
  eventType: EventType.ChangeHealth;
  data: ChangeData;
}
