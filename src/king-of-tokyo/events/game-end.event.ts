import { KotEvent, EventType } from './event';

export interface GameEndEvent extends KotEvent {
  eventType: EventType.GameEnd;
  data: {
    player?: string;
    isTie: boolean;
  };
}
