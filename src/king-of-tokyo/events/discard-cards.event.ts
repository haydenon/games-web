import { KotEvent, EventType } from './event';
import { GameCard } from '../models/game-card';

export interface DiscardCardsEvent extends KotEvent {
  eventType: EventType.DiscardCards;
  data: {
    cardStore: GameCard[];
  };
}
