import { KotEvent, EventType } from './event';

export interface BuyCardRequestEvent extends KotEvent {
  eventType: EventType.BuyCardRequest;
  data: {
    player: string;
  };
}
