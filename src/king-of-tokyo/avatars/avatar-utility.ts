import { AvatarType } from './avatar-type';

export const displayName = (type: AvatarType) => {
  switch (type) {
    case AvatarType.Alienoid:
      return 'Alienoid';
    case AvatarType.CyberKitty:
      return 'Cyber Kitty';
    case AvatarType.Gigazaur:
      return 'Gigazaur';
    case AvatarType.MekaDragon:
      return 'Meka Dragon';
    case AvatarType.SpacePenguin:
      return 'Space Penguin';
    case AvatarType.TheKing:
    default:
      return 'The King';
  }
};

export const imageName = (type: AvatarType) => {
  switch (type) {
    case AvatarType.Alienoid:
      return 'alienoid';
    case AvatarType.CyberKitty:
      return 'cyber_kitty';
    case AvatarType.Gigazaur:
      return 'gigazaur';
    case AvatarType.MekaDragon:
      return 'meka_dragon';
    case AvatarType.SpacePenguin:
      return 'space_penguin';
    case AvatarType.TheKing:
    default:
      return 'the_king';
  }
};

export const className = (type: AvatarType | null) => {
  switch (type) {
    case AvatarType.Alienoid:
      return 'alienoid';
    case AvatarType.CyberKitty:
      return 'cyber-kitty';
    case AvatarType.Gigazaur:
      return 'gigazaur';
    case AvatarType.MekaDragon:
      return 'meka-dragon';
    case AvatarType.SpacePenguin:
      return 'space-penguin';
    case AvatarType.TheKing:
      return 'the-king';
    default:
      return 'none';
  }
};
