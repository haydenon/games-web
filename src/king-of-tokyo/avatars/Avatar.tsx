import React from 'react';

import { Image } from 'react-bootstrap';
import { AvatarType } from './avatar-type';
import { className, displayName, imageName } from './avatar-utility';

import './avatar.scss';

type AvatarSize = 'tiny' | 'compact' | 'default';

interface AvatarArgs {
  avatarType?: AvatarType | null;
  size?: AvatarSize;
}

const Avatar: React.FC<AvatarArgs> = ({ avatarType, size }) => {
  const sizeClass = `player__avatar--${size || 'default'}`;
  return avatarType ? (
    <Image
      className={`player__avatar player__avatar--${className(
        avatarType,
      )} ${sizeClass}`}
      src={`/kot/avatars/${imageName(avatarType)}.png`}
      alt={displayName(avatarType)}
      roundedCircle
    />
  ) : (
    <div className={`player__avatar player__avatar--none ${sizeClass}`} />
  );
};

export default Avatar;
