import React from 'react';

import './avatar-picker.scss';

import { createChangeAvatar } from '../actions/change-avatar.action';
import { createHelpers } from '../king-of-tokyo.helpers';
import { useKingOfTokyo } from '../king-of-tokyo.hook';
import { AvatarType } from './avatar-type';
import { className, displayName, imageName } from './avatar-utility';

const AvatarPicker = () => {
  const { kingOfTokyo, dispatch, isExecuting } = useKingOfTokyo();
  const { others, yourId, player } = createHelpers(kingOfTokyo);
  const otherAvatars = others()
    .filter((p) => p.avatar)
    .map((p) => p.avatar);

  const yourAvatar = player(yourId())?.avatar;
  const all = Object.values(AvatarType) as AvatarType[];
  const available = all.filter((avatar) =>
    otherAvatars.every((other) => other !== avatar),
  );

  const changeAvatar = (avatar?: AvatarType) => {
    dispatch(
      createChangeAvatar(avatar && avatar !== yourAvatar ? avatar : null),
    );
  };

  return (
    <div className="avatars__picker">
      {all.map((avatar) => {
        const isAvailable = available.includes(avatar);
        return (
          <button
            className={`picker__display picker__display--${className(avatar)}${
              !isAvailable ? ' picker__display--unavailable' : ''
            }`}
            onClick={() => changeAvatar(avatar)}
            disabled={!isAvailable || isExecuting}
            key={avatar}
          >
            <img
              src={`/kot/avatars/${imageName(avatar)}.png`}
              alt={displayName(avatar)}
              className="display__image"
            />
          </button>
        );
      })}
    </div>
  );
};

export default AvatarPicker;
