export enum AvatarType {
  Alienoid = 'Alienoid',
  CyberKitty = 'CyberKitty',
  Gigazaur = 'Gigazaur',
  MekaDragon = 'MekaDragon',
  SpacePenguin = 'SpacePenguin',
  TheKing = 'TheKing',
}
