import React from 'react';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './attribute-stat.scss';

export enum Attribute {
  VictoryPoints,
  HitPoints,
  Energy,
}

interface AttributeArg {
  attribute: Attribute;
  value: number;
}

const attributeClass = (attribute: Attribute): string => {
  switch (attribute) {
    case Attribute.VictoryPoints:
      return 'attribute__victory';
    case Attribute.HitPoints:
      return 'attribute__health';
    case Attribute.Energy:
      return 'attribute__energy';
  }
};

const icon = (attribute: Attribute): IconProp => {
  switch (attribute) {
    case Attribute.VictoryPoints:
      return 'star';
    case Attribute.HitPoints:
      return 'heart';
    case Attribute.Energy:
      return 'bolt';
  }
};

const AttributeStat: React.FC<AttributeArg> = ({ attribute, value }) => {
  const attrClass = attributeClass(attribute);
  return (
    <div className="kot__attribute">
      <div className={attrClass}>
        <span className="attribute__icon">
          <FontAwesomeIcon icon={icon(attribute)} />{' '}
        </span>
        <span className="attribute__value">{value}</span>
      </div>
    </div>
  );
};

export default AttributeStat;
