import React from 'react';

import './player-states.scss';

import { useKingOfTokyo } from '../king-of-tokyo.hook';
import { createHelpers } from '../king-of-tokyo.helpers';
import { KingOfTokyoPlayer } from '../king-of-tokyo';
import AttributeStat, { Attribute } from './state/AttributeStat';
import { Card } from 'react-bootstrap';
import Inventory from './Inventory';
import Avatar from '../avatars/Avatar';

interface PlayerArg {
  player: KingOfTokyoPlayer;
}

const PlayerState: React.FC<PlayerArg> = ({ player }) => {
  const { kingOfTokyo } = useKingOfTokyo();
  const { playerState, turnPlayer, isDead } = createHelpers(kingOfTokyo);
  const state = playerState(player.id);

  let selectedClass = '';
  if (turnPlayer() === player.id) {
    selectedClass = ' player__state--turn';
  } else if (isDead(player.id)) {
    selectedClass = ' player__state--dead';
  }

  return (
    <Card className={`player__state${selectedClass}`}>
      <Card.Body className="player__state-container">
        <div className="player__name-container">
          <div className="player__name">
            {player.name}
            {isDead(player.id) ? (
              <span className="dead__status"> (dead)</span>
            ) : null}
          </div>
          <div className="player-state__avatar--small">
            <Avatar avatarType={player.avatar} size="tiny" />
          </div>
        </div>
        <div className="state__body">
          <div className="state__attributes">
            <AttributeStat
              attribute={Attribute.VictoryPoints}
              value={state.victoryPoints}
            />
            <AttributeStat
              attribute={Attribute.HitPoints}
              value={state.hitPoints}
            />
            <AttributeStat attribute={Attribute.Energy} value={state.energy} />
          </div>
          <div>
            <Inventory playerId={player.id} />
          </div>
        </div>
        <div className="player-state__avatar">
          <Avatar avatarType={player.avatar} />
        </div>
      </Card.Body>
    </Card>
  );
};

const PlayerStates: React.FC = () => {
  const { kingOfTokyo } = useKingOfTokyo();
  const { others } = createHelpers(kingOfTokyo);

  return (
    <div className="kot__player-states">
      {others().map((p) => (
        <PlayerState key={p.id} player={p}></PlayerState>
      ))}
    </div>
  );
};

export default PlayerStates;
