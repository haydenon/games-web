import React from 'react';

import './inventory.scss';

import { useKingOfTokyo } from '../king-of-tokyo.hook';
import { createHelpers } from '../king-of-tokyo.helpers';
import GameCardDisplay, { CardSize } from './controls/GameCard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Modal } from 'react-bootstrap';
import { CardType } from '../models/card-type';
import { useModalState } from '../ModalProvider';
import { ModalMode } from '../modal';
import { useScreen, ScreenSize } from '../../util/screen.hook';

interface InventoryArgs {
  playerId: string;
}

const LARGE_VIEW_NUM = 2;
const SMALL_VIEW_NUM = 1;

interface ModalArgs {
  showCards: boolean;
  onHide: () => void;
  player: string;
  cards: CardType[];
}

const InventoryModal: React.FC<ModalArgs> = ({
  onHide,
  showCards,
  player,
  cards,
}) => {
  return (
    <Modal show={showCards} onHide={onHide}>
      <Modal.Header closeButton={true}>
        <Modal.Title>{player} cards</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="inventory__modal">
          {cards.map((c, i) => (
            <span className="card__item" key={i}>
              <GameCardDisplay card={c} />
            </span>
          ))}
        </div>
      </Modal.Body>
    </Modal>
  );
};

const Inventory: React.FC<InventoryArgs> = ({ playerId }) => {
  const { kingOfTokyo } = useKingOfTokyo();
  const {
    modalMode,
    setModeWithContext,
    closeModal,
    contextId,
  } = useModalState();
  const { inventory, playerName } = createHelpers(kingOfTokyo);
  const { breakpoint } = useScreen();

  const show = () => {
    setModeWithContext(ModalMode.Inventory, playerId);
  };

  const inv = inventory(playerId);
  const viewNum =
    breakpoint >= ScreenSize.Xxl ? LARGE_VIEW_NUM : SMALL_VIEW_NUM;
  const displayCards = inv.slice(0, viewNum);
  const remainder = inv.length - viewNum;

  if (inv.length <= 0) {
    return null;
  }

  return (
    <div className="player__inventory">
      <InventoryModal
        player={playerName(playerId) || ''}
        showCards={modalMode === ModalMode.Inventory && contextId === playerId}
        onHide={closeModal}
        cards={inv}
      />
      {displayCards.map((c, i) => (
        <span key={i} className="inventory__item">
          <GameCardDisplay card={c} size={CardSize.Small} />
        </span>
      ))}
      {remainder > 0 ? (
        <span className="inventory__item inventory__remainder">
          +{remainder}
        </span>
      ) : null}
      <Button
        size="sm"
        disabled={modalMode !== ModalMode.Closed}
        onClick={show}
        className="inventory__expand"
        variant="outline-info"
      >
        <FontAwesomeIcon icon="expand-alt" />
      </Button>
    </div>
  );
};

export default Inventory;
