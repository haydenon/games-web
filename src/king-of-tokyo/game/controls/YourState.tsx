import React from 'react';
import { Card } from 'react-bootstrap';

import './your-state.scss';

import { useKingOfTokyo } from '../../king-of-tokyo.hook';
import { createHelpers } from '../../king-of-tokyo.helpers';
import AttributeStat, { Attribute } from '../state/AttributeStat';
import Inventory from '../Inventory';
import Avatar from '../../avatars/Avatar';
import { ScreenSize, useScreen } from '../../../util/screen.hook';

const YourState: React.FC = () => {
  const { kingOfTokyo } = useKingOfTokyo();
  const {
    playerName,
    player,
    yourId,
    playerState,
    yourTurn,
    isDead,
  } = createHelpers(kingOfTokyo);

  const you = yourId() || '';
  const yourAvatar = player(you)?.avatar;
  const yourState = playerState(you);

  const { breakpoint } = useScreen();
  const largeBreakpoint = breakpoint >= ScreenSize.Xxl;

  let selectedClass = '';

  if (yourTurn()) {
    selectedClass = ' status__container--turn';
  } else if (isDead(you)) {
    selectedClass = ' status__container--dead';
  }

  return (
    <div className="control__status">
      <Card className={`status__container${selectedClass}`}>
        <Card.Body>
          <div className="self__name--container">
            <div className="self__name">
              {playerName(you)}
              {isDead(you) ? (
                <span className="dead__status"> (dead)</span>
              ) : null}
            </div>
            <Avatar
              avatarType={yourAvatar}
              size={largeBreakpoint ? 'default' : 'compact'}
            />
          </div>
          <div className="state__body">
            <div className="state__attributes">
              <AttributeStat
                attribute={Attribute.VictoryPoints}
                value={yourState.victoryPoints}
              />
              <AttributeStat
                attribute={Attribute.HitPoints}
                value={yourState.hitPoints}
              />
              <AttributeStat
                attribute={Attribute.Energy}
                value={yourState.energy}
              />
            </div>
            <div>
              <Inventory playerId={you} />
            </div>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
};

export default YourState;
