import React from 'react';

import './control-bar.scss';

import DiceRollList from './DiceRollList';
import CardStore from './CardStore';
import YourState from './YourState';

const ControlBar: React.FC = () => {
  return (
    <div className="kot__controls">
      <div className="state-dice__container">
        <div className="state-dice__contents">
          <YourState />
          <div className="controls__dice">
            <DiceRollList></DiceRollList>
          </div>
        </div>
      </div>
      <div className="controls__store">
        <CardStore></CardStore>
      </div>
    </div>
  );
};

export default ControlBar;
