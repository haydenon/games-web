import React, { useState, useEffect } from 'react';
import { Button, Card, Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './card-store.scss';

import GameCardDisplay, { CardSize } from './GameCard';
import { useKingOfTokyo } from '../../king-of-tokyo.hook';
import { createHelpers } from '../../king-of-tokyo.helpers';
import { StateType } from '../../state/state';
import { createFinishCardBuying } from '../../actions/finish-card-buying.action';
import { createDiscardCards } from '../../actions/discard-cards.action';
import { CardType } from '../../models/card-type';
import { createBuyCard } from '../../actions/buy-card.action';
import { useScreen, ScreenSize } from '../../../util/screen.hook';
import { useModalState } from '../../ModalProvider';
import { ModalMode } from '../../modal';
import { GameCard } from '../../models/game-card';

interface ActionArgs {
  disabled: boolean;
  buyEnabled: boolean;
}

interface ModalArgs {
  showCards: boolean;
  onHide: () => void;
  cards: GameCard[];
  disabled: boolean;
  buyEnabled: boolean;
  onBuy: (type: CardType) => void;
}

const StoreModal: React.FC<ModalArgs> = ({
  onHide,
  showCards,
  cards,
  buyEnabled,
  disabled,
  onBuy,
}) => {
  const { kingOfTokyo } = useKingOfTokyo();
  const { playerState, yourId } = createHelpers(kingOfTokyo);
  const yourState = playerState(yourId());

  return (
    <Modal
      show={showCards}
      onHide={onHide}
      dialogClassName="store__modal"
      centered={true}
    >
      <Modal.Header closeButton={true}>
        <Modal.Title>Store</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="store__cards">
          {cards.map((c, i) => (
            <span className="card__item" key={i}>
              <GameCardDisplay
                card={c}
                onBuy={() => onBuy(c.cardType)}
                buyEnabled={buyEnabled && c.energyCost <= yourState.energy}
              />
            </span>
          ))}
        </div>
        <div className="store__actions">
          <CardActions buyEnabled={buyEnabled} disabled={disabled} />
        </div>
      </Modal.Body>
    </Modal>
  );
};

const CardActions: React.FC<ActionArgs> = ({ disabled, buyEnabled }) => {
  const { dispatch } = useKingOfTokyo();

  const skip = () => {
    dispatch(createFinishCardBuying());
  };
  const discard = () => {
    dispatch(createDiscardCards());
  };

  return buyEnabled ? (
    <>
      <Button variant="success" disabled={disabled} onClick={skip}>
        Skip buying
      </Button>
      <Button variant="outline-danger" disabled={disabled} onClick={discard}>
        Discard (2 <FontAwesomeIcon icon="bolt" />)
      </Button>
    </>
  ) : null;
};

function cardSize(width: number) {
  if (width >= ScreenSize.Xxl) {
    return CardSize.Large;
  }

  return width >= 710 ? CardSize.Medium : CardSize.Small;
}

const CardStore: React.FC = () => {
  const { kingOfTokyo, isExecuting, dispatch } = useKingOfTokyo();
  const { cardStore, isState, playerState, yourId, yourTurn } = createHelpers(
    kingOfTokyo,
  );
  const { breakpoint, width } = useScreen();
  const { modalMode, closeModal, setMode } = useModalState();
  const [isBuyState, setIsBuyState] = useState(false);
  const isBuyEnabled = isState(StateType.BuyCards) && yourTurn();
  const largeBreakpoint = breakpoint >= ScreenSize.Xxl;

  useEffect(() => {
    setIsBuyState(isBuyEnabled);
  }, [isBuyEnabled]);

  useEffect(() => {
    if (isBuyState) {
      setMode(ModalMode.CardStore);
    } else {
      closeModal();
    }
  }, [isBuyState, setMode, closeModal]);

  const yourState = playerState(yourId());
  const cards = cardStore();

  const buy = (type: CardType) => {
    dispatch(createBuyCard(type));
  };
  const openStore = () => {
    setMode(ModalMode.CardStore);
  };

  const actions = !largeBreakpoint ? (
    <Button
      variant="outline-info"
      className="store__expand"
      onClick={openStore}
      disabled={modalMode !== ModalMode.Closed}
    >
      Open store <FontAwesomeIcon icon="expand-alt" />
    </Button>
  ) : (
    <CardActions buyEnabled={isBuyEnabled} disabled={isExecuting} />
  );

  return (
    <>
      <Card className="store__content">
        <Card.Body>
          <div className="store__cards">
            {cards.map((c, i) => (
              <GameCardDisplay
                disabled={isExecuting}
                buyEnabled={
                  largeBreakpoint &&
                  isBuyEnabled &&
                  c.energyCost <= yourState.energy &&
                  modalMode !== ModalMode.CardStore
                }
                onBuy={() => buy(c.cardType)}
                size={cardSize(width)}
                key={i}
                card={c}
              ></GameCardDisplay>
            ))}
          </div>
          <div className="store__actions">{actions}</div>
        </Card.Body>
      </Card>
      <StoreModal
        showCards={modalMode === ModalMode.CardStore}
        cards={cards}
        onBuy={(type) => buy(type)}
        onHide={closeModal}
        buyEnabled={isBuyEnabled}
        disabled={isExecuting}
      />
    </>
  );
};

export default CardStore;
