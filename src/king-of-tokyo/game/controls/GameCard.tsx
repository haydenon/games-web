import React from 'react';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './game-card.scss';

import { GameCard } from '../../models/game-card';
import { CardType } from '../../models/card-type';

export enum CardSize {
  Small = 'sm',
  Medium = 'md',
  Large = 'lg',
}

interface CardArgs {
  card: GameCard | CardType;
  disabled?: boolean;
  buyEnabled?: boolean;
  size?: CardSize;
  onBuy?: () => void;
}

const sizeClass = (size?: CardSize) => {
  if (!size) {
    return '';
  }

  switch (size) {
    case CardSize.Small:
      return ' kot__card--small';
    case CardSize.Medium:
      return ' kot__card--med';
    default:
      return '';
  }
};

const GameCardDisplay: React.FC<CardArgs> = ({
  card,
  disabled,
  buyEnabled,
  size,
  onBuy,
}) => {
  const type = typeof card === 'string' ? card : card.cardType;
  const energy = typeof card === 'string' ? 0 : card.energyCost;
  return (
    <div className={`kot__card${sizeClass(size)}`}>
      <img src={`/kot/cards/${type}.jpg`} alt={`Buy ${type}`} />
      {buyEnabled ? (
        <div className="card__buy">
          <Button
            className="buy__button"
            variant="outline-success"
            size="lg"
            disabled={disabled}
            onClick={() => (onBuy ? onBuy() : undefined)}
          >
            Buy ({energy} <FontAwesomeIcon icon="bolt" />)
          </Button>
        </div>
      ) : null}
    </div>
  );
};

export default GameCardDisplay;
