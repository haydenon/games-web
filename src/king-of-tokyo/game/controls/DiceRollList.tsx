import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

import './dice-roll-list.scss';

import { DiceRollType } from '../../models/dice-roll';
import Claw from '../../resources/Claw';
import { useKingOfTokyo } from '../../king-of-tokyo.hook';
import { createHelpers } from '../../king-of-tokyo.helpers';
import { Button, Card } from 'react-bootstrap';
import { StateType } from '../../state/state';
import { createRollDice } from '../../actions/roll.action';
import { createAcceptDice } from '../../actions/accept-dice.action';
import { useScreen, ScreenSize } from '../../../util/screen.hook';

interface DiceTypeArg {
  dice: DiceRollType;
}

interface DiceArg extends DiceTypeArg {
  isSelected: boolean;
  isExtra: boolean;
  onSelection: () => void;
  disabled: boolean;
}

const numLabel = (dice: DiceRollType) => {
  switch (dice) {
    case DiceRollType.One:
      return 1;
    case DiceRollType.Two:
      return 2;
    case DiceRollType.Three:
    default:
      return 3;
  }
};

const iconName = (dice: DiceRollType): IconProp => {
  if (dice === DiceRollType.Heart) {
    return 'heart';
  }

  return 'bolt';
};

const DiceContent: React.FC<DiceTypeArg> = ({ dice }) => {
  switch (dice) {
    case DiceRollType.One:
    case DiceRollType.Two:
    case DiceRollType.Three:
      return <>{numLabel(dice)}</>;
    case DiceRollType.Heart:
    case DiceRollType.Energy:
      return <FontAwesomeIcon icon={iconName(dice)}></FontAwesomeIcon>;
    case DiceRollType.Claw:
      return <Claw></Claw>;
  }
};

const Dice: React.FC<DiceArg> = ({
  dice,
  isSelected,
  onSelection,
  disabled,
  isExtra,
}) => {
  return (
    <button
      disabled={disabled}
      onClick={onSelection}
      className={`dice__roll${isSelected ? ' dice__roll--selected' : ''}${
        isExtra ? ' dice__roll--extra' : ''
      }`}
    >
      {<DiceContent dice={dice} />}
    </button>
  );
};

interface ActionArgs {
  disabled: boolean;
  hidden: boolean;
  selection: boolean[];
}

const DiceActions: React.FC<ActionArgs> = ({ selection, hidden, disabled }) => {
  const { kingOfTokyo, dispatch } = useKingOfTokyo();
  const { isState } = createHelpers(kingOfTokyo);
  const { breakpoint } = useScreen();

  const roll = () => {
    dispatch(createRollDice(selection));
  };

  const accept = () => {
    dispatch(createAcceptDice());
  };

  const isSmall = breakpoint <= ScreenSize.Md;

  const keepCount = selection.filter((s) => s).length;
  const rerollCount = selection.length - keepCount;
  const actionText =
    keepCount === 0
      ? 'Reroll all dice'
      : `Reroll ${rerollCount} ${rerollCount > 1 ? 'dice' : 'die'}`;

  if (hidden) {
    return null;
  }

  if (isState(StateType.PlayerRoll)) {
    return (
      <div className="dice__actions dice__actions--roll">
        <Button
          variant="outline-dark"
          onClick={accept}
          disabled={disabled}
          size={isSmall ? 'sm' : undefined}
        >
          Finish roll
        </Button>
        {rerollCount !== 0 ? (
          <Button
            variant="success"
            onClick={roll}
            disabled={disabled}
            size={isSmall ? 'sm' : undefined}
          >
            {actionText}
          </Button>
        ) : null}
      </div>
    );
  }

  if (isState(StateType.PlayerRollStart)) {
    return (
      <div className="dice__actions dice__actions--start">
        <Button variant="success" onClick={roll} disabled={disabled}>
          Roll dice
        </Button>
      </div>
    );
  }

  return null;
};

const DiceRollList: React.FC = () => {
  const { kingOfTokyo, isExecuting } = useKingOfTokyo();
  const { diceRoll, rollCount, yourTurn } = createHelpers(kingOfTokyo);
  const diceRolls = diceRoll();
  const rollNumber = rollCount();
  const currentRollNum = useRef(rollNumber);
  const defaultSelection = diceRolls.map(() => false);
  const [diceSelections, setSelection] = useState(defaultSelection);

  useEffect(() => {
    if (currentRollNum.current !== rollNumber) {
      const updated =
        rollNumber >= 0
          ? diceRolls.map((r) => r.wasPreserved)
          : diceRolls.map(() => false);
      setSelection(updated);
      currentRollNum.current = rollNumber;
    }
  }, [diceRolls, rollNumber]);

  const handleSelection = (ind: number) => () => {
    const updated = [...diceSelections];
    updated[ind] = !updated[ind];
    setSelection(updated);
  };

  const isYourTurn = yourTurn();

  return (
    <div className="dice__container">
      <Card className="dice__content">
        <Card.Body>
          {rollNumber >= 0 ? (
            <div className="dice__roll-num small">
              Roll# <span className="number__count">{rollNumber + 1}</span>
            </div>
          ) : null}
          <div>
            {diceRolls.map((d, i) => (
              <Dice
                disabled={rollNumber < 1 || !isYourTurn || isExecuting}
                isSelected={diceSelections[i]}
                isExtra={i >= 6}
                onSelection={handleSelection(i)}
                key={i}
                dice={d.diceRoll}
              ></Dice>
            ))}
          </div>
          <DiceActions
            disabled={isExecuting}
            hidden={!isYourTurn}
            selection={diceSelections}
          ></DiceActions>
        </Card.Body>
      </Card>
    </div>
  );
};

export default DiceRollList;
