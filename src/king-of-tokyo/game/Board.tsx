import React from 'react';

import './board.scss';

import { useKingOfTokyo } from '../king-of-tokyo.hook';
import { createHelpers } from '../king-of-tokyo.helpers';
import { Button, Popover } from 'react-bootstrap';
import { createLeaveTokyo } from '../actions/leave-tokyo.actions';
import { KingOfTokyoPlayer } from '../king-of-tokyo';
import { className } from '../avatars/avatar-utility';

const createClass = (
  city: boolean,
  disabled: boolean,
  player: KingOfTokyoPlayer | undefined,
) =>
  `board__spot board__spot--${city ? 'city' : 'bay'}${
    player ? ` board__spot--${className(player.avatar)}` : ''
  } ${
    disabled
      ? 'board__spot--disabled'
      : player
      ? 'board__spot--occupied'
      : 'board__spot--empty'
  }`;

interface SpotArgs {
  isCity: boolean;
  disabled?: boolean;
}

const SpotActions: React.FC = () => {
  const { dispatch, isExecuting } = useKingOfTokyo();

  const executeLeave = (leave: boolean) => {
    dispatch(createLeaveTokyo(leave));
  };

  return (
    <>
      <Button
        disabled={isExecuting}
        onClick={() => executeLeave(false)}
        className="spot__action"
        variant="success"
      >
        Stay
      </Button>
      <Button
        disabled={isExecuting}
        onClick={() => executeLeave(true)}
        className="spot__action"
        variant="danger"
      >
        Leave
      </Button>
    </>
  );
};

const TokyoSpot: React.FC<SpotArgs> = ({ isCity, disabled }) => {
  const { kingOfTokyo } = useKingOfTokyo();
  const {
    isRequestingTokyoLeave,
    tokyoBayPlayer,
    tokyoCityPlayer,
    yourId,
    player,
  } = createHelpers(kingOfTokyo);

  const cityPlayer = player(tokyoCityPlayer());
  const bayPlayer = player(tokyoBayPlayer());
  const thisPlayer = isCity ? cityPlayer : bayPlayer;

  const spotClass = createClass(
    isCity,
    disabled || false,
    isCity ? cityPlayer : bayPlayer,
  );
  const showActions = isRequestingTokyoLeave() && thisPlayer?.id === yourId();

  return (
    <div className="spot__container">
      <div className={spotClass}>
        <span className="spot__occupant">
          {disabled ? '(disabled)' : thisPlayer?.name || '(empty)'}
        </span>
        <span className="spot__identifier">
          Tokyo {isCity ? 'City' : 'Bay'}
        </span>
      </div>
      {showActions ? (
        <Popover
          className="spot__actions"
          id={`spot-actions-${isCity ? 'city' : 'bay'}`}
          placement="bottom"
        >
          <Popover.Content>
            <SpotActions />
          </Popover.Content>
        </Popover>
      ) : null}
    </div>
  );
};

const Board: React.FC = () => {
  const { kingOfTokyo } = useKingOfTokyo();
  const { alivePlayerCount } = createHelpers(kingOfTokyo);

  const bayEnabled = alivePlayerCount() >= 5;
  return (
    <div className="kot__board">
      <div className="board__spots">
        <TokyoSpot isCity={true}></TokyoSpot>
        <TokyoSpot isCity={false} disabled={!bayEnabled}></TokyoSpot>
      </div>
    </div>
  );
};

export default Board;
