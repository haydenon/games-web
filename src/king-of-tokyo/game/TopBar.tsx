import React, { useMemo } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

import './top-bar.scss';
import { useKingOfTokyo } from '../king-of-tokyo.hook';
import { statusSummary } from '../state/status-summary';

const TopBar: React.FC = () => {
  const { kingOfTokyo } = useKingOfTokyo();
  const status = useMemo(() => statusSummary(kingOfTokyo), [kingOfTokyo]);

  return (
    <div className="kot__top-bar">
      <Link className="top-bar__link--home" to="/">
        <FontAwesomeIcon className="notification__icon" icon="chevron-left" />{' '}
        Back home
      </Link>
      <div className="top-bar__status small">{status}</div>
    </div>
  );
};

export default TopBar;
