import React from 'react';

import './game-styles.scss';

import ControlBar from './controls/ControlBar';
import PlayerStates from './PlayerStates';
import TopBar from './TopBar';
import Board from './Board';
import GameModal from './GameModal';
import ModalProvider from '../ModalProvider';

const Game: React.FC = () => {
  return (
    <ModalProvider>
      <div className="kot__container">
        <TopBar></TopBar>
        <PlayerStates></PlayerStates>
        <Board></Board>
        <ControlBar></ControlBar>
        <GameModal></GameModal>
      </div>
    </ModalProvider>
  );
};

export default Game;
