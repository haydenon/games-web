import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';

import './game-modal.scss';

import { useKingOfTokyo } from '../king-of-tokyo.hook';
import { createHelpers } from '../king-of-tokyo.helpers';
import { StateType } from '../state/state';
import { createStartTurn } from '../actions/start-turn.action';
import { Redirect } from 'react-router-dom';
import { useModalState } from '../ModalProvider';
import { ModalMode } from '../modal';

const PlayerWinModal: React.FC = () => {
  const [leaveGame, setLeave] = useState(false);

  const { kingOfTokyo } = useKingOfTokyo();
  const { playerName } = createHelpers(kingOfTokyo);

  const state = kingOfTokyo?.state;
  const winner =
    (state?.state === StateType.GameEnd && state.winningPlayer) || undefined;
  const winnerName = playerName(winner);
  const isTie = !winner;

  if (leaveGame) {
    return <Redirect to={{ pathname: '/' }} />;
  }

  return (
    <Modal show={true} centered onHide={() => {}}>
      <Modal.Header closeButton={false}>
        <Modal.Title>
          {isTie ? "Ooops, you're all dead 😕" : 'We have a winner! 🥳'}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {isTie
          ? "That's a bit rough, try to show some more love next time!"
          : `Congratulations ${winnerName}! 🎉`}
        <div className="kot-modal__action">
          <Button variant="primary" onClick={() => setLeave(true)}>
            Leave game
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

const PlayerTurnStartModal: React.FC = () => {
  const { isExecuting, dispatch } = useKingOfTokyo();
  const startTurn = () => {
    if (!isExecuting) {
      dispatch(createStartTurn());
    }
  };

  return (
    <Modal show={true} onHide={startTurn} centered>
      <Modal.Header closeButton={false}>
        <Modal.Title>Your turn</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="kot-modal__action">
          <Button disabled={isExecuting} variant="success" onClick={startTurn}>
            Start turn
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

const GameModal: React.FC = () => {
  const { kingOfTokyo } = useKingOfTokyo();
  const { isState, yourTurn } = createHelpers(kingOfTokyo);
  const { modalMode, setMode, closeModal } = useModalState();

  const isYourTurnStart = yourTurn() && isState(StateType.PlayerTurnStart);
  const isGameEnd = isState(StateType.GameEnd);

  useEffect(() => {
    if (isYourTurnStart && modalMode !== ModalMode.PlayerTurnStart) {
      setMode(ModalMode.PlayerTurnStart);
    } else if (isGameEnd && modalMode !== ModalMode.GameEnd) {
      setMode(ModalMode.GameEnd);
    } else if (!isYourTurnStart && modalMode === ModalMode.PlayerTurnStart) {
      closeModal();
    }
  }, [modalMode, setMode, closeModal, isYourTurnStart, isGameEnd]);

  if (modalMode === ModalMode.PlayerTurnStart) {
    return <PlayerTurnStartModal />;
  } else if (modalMode === ModalMode.GameEnd) {
    return <PlayerWinModal />;
  } else {
    return null;
  }
};

export default GameModal;
