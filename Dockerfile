FROM node:14@sha256:56cb8585b21a1da7250d0930d8221a1e35c528e51d937304131f0ea9233e756b AS build-env
WORKDIR /app

RUN npm install node-sass
RUN npm rebuild node-sass

COPY ./package.json ./
COPY ./package-lock.json ./

RUN npm install

COPY . ./
RUN npm run build

FROM nginx:1.18.0-alpine@sha256:2668e65e1a36a749aa8b3a5297eee45504a4efea423ec2affcbbf85e31a9a571 AS bin
WORKDIR /app

COPY --from=build-env /app/build .
COPY ./nginx.conf /etc/nginx/nginx.conf
